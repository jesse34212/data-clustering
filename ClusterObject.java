
import java.util.List;

public class ClusterObject implements Comparable<ClusterObject>{
	private List<DataObject> cluster;
	private DataObject averageDataObject;
	
	public ClusterObject(List<DataObject> cluster){
		this.cluster = cluster;
		this.averageDataObject = Utils.findAverageDataObject(cluster);
	}
	
	ClusterObject(ClusterObject clusterObject1, ClusterObject clusterObject2){
		this.cluster = clusterObject1.getClusterList();
		addToCluster(clusterObject2);
		this.averageDataObject = Utils.findAverageDataObject(cluster);
	}
	
	public List<DataObject> getClusterList(){
		return this.cluster;
	}
	
	public void addToCluster(ClusterObject clusterObject){
		for (int i = 0; i < clusterObject.getClusterList().size(); i ++){
			this.cluster.add(clusterObject.getClusterList().get(i));
		}
		this.averageDataObject = Utils.findAverageDataObject(this.cluster);
	}
	
	public DataObject getAverageData(){
		return averageDataObject;
	}

	public int compareTo(ClusterObject o) {
		
		return 0;
	}
	public void removeFakePoints(){
		for (int i = 0;  i<  cluster.size(); i++){
			if (cluster.get(i).getLineNumber() > 5000){
				cluster.remove(i);
			}
		}
	}
}
