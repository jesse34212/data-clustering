
import java.util.ArrayList;
import java.util.List;

public class ClustersContainerObject {
	
	List<List<DataObject>> data;
	List<ClusterObject> clusters;
	private static int HIGH_START_COST = 99999999;
	double cost = HIGH_START_COST;
	
	public ClustersContainerObject(List<ClusterObject> clusters, boolean isAverageLinkage){
		if (isAverageLinkage){
			this.clusters = clusters;
		}
	}
	
	public ClustersContainerObject(List<List<DataObject>> data){
		clusters = new ArrayList<ClusterObject>();
		for (List<DataObject> cluster : data){
			clusters.add(new ClusterObject(cluster));
		}
		this.data = data;
	}
	
	public double getKmeansCost(){
		//only compute Kmeans if cost is not already set.
		if (cost > HIGH_START_COST - 1){
			cost =  Utils.computeKMeansCost(data);
		}
		return cost;
	}
	
	public List<ClusterObject> getClusters(){
		return this.clusters;
	}
	
	public int getSize(){
		int size = 0;
		for (ClusterObject cluster : clusters){
			for (DataObject object : cluster.getClusterList()){
				size++;
			}
		}
		return size;
	}
	
	public void setAllClusterAssignments(){
		for (int i = 0; i < clusters.size(); i++){
			for (int j = 0; j< clusters.get(i).getClusterList().size(); j++){
				clusters.get(i).getClusterList().get(j).setclusterAssociation(i);
			}
		}
	}
}
