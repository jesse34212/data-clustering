
import java.math.BigDecimal;

public class HammingDistance {

	private ClustersContainerObject linkageClusterContainer;
	private ClustersContainerObject lloydsClusterContainer;

	public HammingDistance(AverageLinkageImpl linkage,
			KmeansAlgorithmImpl lloyds) {
		this.lloydsClusterContainer = lloyds.getClusterContainerObject();
		this.linkageClusterContainer = linkage.getClusterContainerObject();
		doHammingDistance(totalMisses(),
				nChooseTwo(linkageClusterContainer.getSize()).doubleValue());
	}

	private void doHammingDistance(double numerator, double denominator) {
		System.out.print("Hamming Distance = " + (numerator / denominator) + "\n");
	}

	private double totalMisses() {
		int totalMisses = 0;
		for (ClusterObject clusters : linkageClusterContainer.getClusters()) {
			for (int i = 0; i < clusters.getClusterList().size() - 1; i++) {
				for (int j = i + 1; j < clusters.getClusterList().size(); j++) {
					if ((!(clusters.getClusterList().get(i).getLineNumber() == Utils.FAKE_POINT))
							&& (!(clusters.getClusterList().get(j)
									.getLineNumber() == Utils.FAKE_POINT))) {
						totalMisses += isContainedInLloydsClusters(clusters
								.getClusterList().get(i), clusters
								.getClusterList().get(j)) ? 0 : 1;
					}
				}
			}
		}

		for (ClusterObject clusters : lloydsClusterContainer.getClusters()) {
			for (int i = 0; i < clusters.getClusterList().size() - 1; i++) {
				for (int j = i + 1; j < clusters.getClusterList().size(); j++) {
					if ((!(clusters.getClusterList().get(i).getLineNumber() == Utils.FAKE_POINT))
							&& (!(clusters.getClusterList().get(j)
									.getLineNumber() == Utils.FAKE_POINT))) {
						totalMisses += isContainedInLinkageClusters(clusters
								.getClusterList().get(i), clusters
								.getClusterList().get(j)) ? 0 : 1;
					}
				}
			}
		}
		return (double) totalMisses;
	}

	private boolean isContainedInLloydsClusters(DataObject object1,
			DataObject object2) {
		boolean result = false;
		for (ClusterObject clusters : lloydsClusterContainer.getClusters()) {
			result = Utils.clusterContainsObjects(object1, object2,
					clusters.getClusterList())
					|| result;
			if (result) {
				break;
			}
		}
		return result;
	}

	private boolean isContainedInLinkageClusters(DataObject object1,
			DataObject object2) {
		boolean result = false;
		for (ClusterObject clusters : linkageClusterContainer.getClusters()) {
			result = Utils.clusterContainsObjects(object1, object2,
					clusters.getClusterList())
					|| result;
			if (result) {
				break;
			}
		}
		return result;
	}

	private BigDecimal nChooseTwo(int n) {
		return (factorial(n).divide((factorial(2).multiply(factorial(n - 2)))));
	}

	private BigDecimal factorial(int fact) {
		BigDecimal factorial = new BigDecimal(fact);
		while (fact > 1) {
			factorial = factorial.multiply(new BigDecimal(--fact));
		}
		return factorial;
	}
}
