
import java.io.IOException;
import java.util.List;

public class Activator {
	private String filePath;
	private int kClusters;
	private static List<DataObject> dataList;
	private static final String KMEANS = "kmeans";
	private static final String AVERAGE = "average";
	private static final String HAM = "ham";

	private Activator(String[] args) {
		filePath = args[0];
		kClusters = Integer.parseInt(args[1]);
		try {
			dataList = Utils.parseDataFile(filePath);
			if (KMEANS.equals(args[2])) {
				KmeansAlgorithmImpl Lloyds = new KmeansAlgorithmImpl(kClusters,
						dataList);
			} else if (AVERAGE.equals(args[2])) {
				AverageLinkageImpl average = new AverageLinkageImpl(kClusters,
						dataList);
			} else if (HAM.equals(args[2])) {
				KmeansAlgorithmImpl Lloyds = new KmeansAlgorithmImpl(kClusters,
						dataList);
				AverageLinkageImpl average = new AverageLinkageImpl(kClusters,
						dataList);
				HammingDistance ham = new HammingDistance(average, Lloyds);
			} else {
				System.out.println("Invalid algorithm name: " + args[2]);
				System.out
						.println("Plese try again by entering \"kmeans\", \"average\", or \"ham\"");
				System.out
						.println("\"average\" will run the average-linkage algorithm");
				System.out
						.println("\"kmeans\" will run Lloyd\'s kmeans heuristic algorithm");
				System.out
						.println("\"ham\" will run both Lloyd\'s algorithm and the average-linkage algorithm and will report the hamming distance between the two.");

			}
		} catch (IOException e) {
			System.out.println("IOException: ");
			e.printStackTrace();
		}
	}

	public static List<DataObject> getDataList() {
		return dataList;

	}
    
    public static void main(String[] args){
        Activator activator = new Activator(args);
    }
}
