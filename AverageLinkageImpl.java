
import java.util.List;
import java.util.Map;

public class AverageLinkageImpl implements Algorithm{

	/*
	 * Average Linkage - 
	 * 1.)start by making every point a cluster. 
	 * 2.) Go through each point and find the two closest points. 
	 * 3.) Joint the two clusters. 
	 * 4.) Find the average to represent the new cluster
	 */
	
	private int kClusters;
	private List<ClusterObject> clusterList;
	private ClustersContainerObject clusterContainer;
	private long startTimeMillis, stopTimeMillis;
	Map<List<DataObject>, Double> distances;
	
	public AverageLinkageImpl(int kClusters, final List<DataObject> dataList){
		
		this.kClusters = kClusters;
		clusterList = Utils.createClusterList(dataList);
		doAverageLinkage();
		System.out.println("\nTotal operational time for Average-Linkage in millis: " + (stopTimeMillis - startTimeMillis));
		System.out.print("Avg-Li: ");
		Utils.printVectorRepresentation(clusterContainer, kClusters, false);
	}
	
	private void doAverageLinkage(){
		startTimeMillis = System.currentTimeMillis();
		ClusterObject[] combinedClusters;
		while (clusterList.size() > kClusters){
			combinedClusters= Utils.findClosestClusters(this.clusterList);
			clusterList.remove(combinedClusters[0]);
			clusterList.remove(combinedClusters[1]);
			clusterList.add(new ClusterObject(combinedClusters[0], combinedClusters[1]));
		}
		for (int clusterAssociation = 0; clusterAssociation < clusterList.size(); clusterAssociation++){
			for (DataObject dataObject : clusterList.get(clusterAssociation).getClusterList()){
				dataObject.setclusterAssociation(clusterAssociation);
			}
		}
		clusterContainer = new ClustersContainerObject(clusterList, true);
		stopTimeMillis = System.currentTimeMillis();
	}
	
	public ClustersContainerObject getClusterContainerObject(){
		return clusterContainer;
	}
}
