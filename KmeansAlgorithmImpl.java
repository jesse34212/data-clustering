
import java.util.ArrayList;
import java.util.List;

/*
 * 1.) cluster based on random point that is part of the data set
 * 2.) find average data point from each cluster
 * 3.) run until data does not change. this completes 1 of 100 itterations
 */
public class KmeansAlgorithmImpl implements Algorithm{
	int kClusters;
	private List<DataObject> dataList = new ArrayList<DataObject>();
	private ClustersContainerObject clusterContainerObject;

	public KmeansAlgorithmImpl(int kClusters, final List<DataObject> dataList) {
		this.kClusters = kClusters;
		this.dataList = dataList;
		doLloyds();
	}

	private void doLloyds() {
		long startTimeMillis = System.currentTimeMillis();
		ClustersContainerObject bestClustersObject = null;
		ClustersContainerObject worstClustersObject = null;
		List<List<DataObject>> lastClustersList = null;
		List<List<DataObject>> currentClustersList = null;
		List<List<DataObject>> lastLastClustersList = null;
		ClustersContainerObject currentClustersObject;

		for (int i = 0; i < 100; i++) {
			lastClustersList = null;
			currentClustersList = null;
			lastLastClustersList = null;
			currentClustersList = Utils.createClusters(dataList, Utils.createRandomCenters(kClusters, dataList));
			currentClustersList = Utils.createClusters(dataList, Utils.createAverageCenters(currentClustersList));
			while(!Utils.compareClusters(currentClustersList, lastClustersList) || !Utils.compareClusters(currentClustersList, lastLastClustersList)){
				lastLastClustersList = lastClustersList;
				lastClustersList = currentClustersList;
				currentClustersList = Utils.createClusters(dataList, Utils.createAverageCenters(currentClustersList));
			}
			currentClustersObject = new ClustersContainerObject(currentClustersList);
			if (bestClustersObject == null || currentClustersObject.getKmeansCost() < bestClustersObject.getKmeansCost()){
				bestClustersObject = new ClustersContainerObject(currentClustersList);
			} 
			if (worstClustersObject == null || currentClustersObject.getKmeansCost() > worstClustersObject.getKmeansCost()){
				worstClustersObject = new ClustersContainerObject(currentClustersList);
			}
			
			if (i == 99) {
				long stopTimeMillis = System.currentTimeMillis();
				System.out.println("\nTotal operational time for Lloyds in millis: " + (stopTimeMillis - startTimeMillis));
				clusterContainerObject = bestClustersObject;
				removeFakePoints();
				clusterContainerObject.setAllClusterAssignments();
				System.out.print("Lloyds: ");
				Utils.printVectorRepresentation(bestClustersObject, kClusters, true);
			}
		}
	}

	private void removeFakePoints(){
		for (ClusterObject cluster : clusterContainerObject.getClusters()){
			cluster.removeFakePoints();
		}
	}
	public ClustersContainerObject getClusterContainerObject() {
		// TODO Auto-generated method stub
		return clusterContainerObject;
	}
}
