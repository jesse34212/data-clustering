
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {
	static final String TAB_DELIMITER = "\t";
	static final String SPACE_DELIMITER = " ";
	static final int FAKE_POINT = 999999;

	public static double computeKMeansCost(List<List<DataObject>> clusters) {
		/*
		 * The cost is the squared distance between all the points to their
		 * closest cluster center.
		 */
		double cost = 0.0;
		DataObject center;
		for (List<DataObject> cluster : clusters) {
			// first index is the center
			center = cluster.get(0);
			for (DataObject dataObject : cluster) {
				cost += getEuclidianDistance(dataObject.getDimensions(),
						center.getDimensions());
			}
		}
		return cost;
	}

	public static double getEuclidianDistance(List<Double> list1,
			List<Double> list2) {
		double euclidianDistance = 0.0;
		double partialDistance = 0.0;
		for (int i = 0; i < list1.size(); i++) {
			partialDistance += Math.pow(list2.get(i) - list1.get(i), 2);
		}
		euclidianDistance = Math.sqrt(partialDistance);
		return euclidianDistance;
	}

	public static ArrayList<DataObject> parseDataFile(String dataFile) throws IOException {
		ArrayList<DataObject> dataList = new ArrayList<DataObject>();
		BufferedReader br = null;
		try {
			String currentLine;
			br = new BufferedReader(new FileReader(dataFile));
			try {
				int lineNumber = 0;
				while ((currentLine = br.readLine()) != null) {
					dataList.add(createDataObject(currentLine, lineNumber++));
				}
			} catch (IOException e) {
				throw e;
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found at filepath: " + dataFile);
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dataList;
	}

	private static DataObject createDataObject(String currentLine,
			int lineNumber) {
		String[] dataValues = null;
		dataValues = currentLine
				.split(currentLine.contains(TAB_DELIMITER) ? TAB_DELIMITER
						: SPACE_DELIMITER);
		ArrayList<Double> dataPoints = new ArrayList<Double>();
		for (int i = 0; i < dataValues.length; i++) {
			dataPoints.add(Double.parseDouble(dataValues[i]));
		}
		return new DataObject(dataPoints, lineNumber);
	}

	static List<List<DataObject>> createRandomCenters(int kClusters,
			List<DataObject> dataList) {
		List<List<DataObject>> clusters = new ArrayList<List<DataObject>>();
		List<Integer> chosenInts = new ArrayList<Integer>();
		for (int i = 0; i < kClusters; i++) {
			int random = randInt(dataList.size() - 1);
			while (chosenInts.contains(random)) {
				random = randInt(dataList.size() - 1);
			}
			chosenInts.add(random);
			DataObject object = dataList.get(random);
			object.setclusterAssociation(i);
			ArrayList<DataObject> objects = new ArrayList<DataObject>();
			objects.add(object);
			clusters.add(objects);
		}
		return clusters;
	}

	public static int randInt(int max) {
		return new Random().nextInt((max) + 1);
	}

	public static List<List<DataObject>> createClusters(
			List<DataObject> dataList, List<List<DataObject>> centers) {
		List<List<DataObject>> clusters = centers;
		for (DataObject object : dataList) {
			// sqrt((this.d1-that.d1)^2 + (this.d2-that.d2)^2...+
			// (this.dx-that.dx)^2)
			ArrayList<Double> comparedValues = new ArrayList<Double>();
			for (List<DataObject> center : clusters) {
				comparedValues.add(getEuclidianDistance(center.get(0)
						.getDimensions(), object.getDimensions()));
			}
			// smallest index in the comparator structure indicates the closest
			// center and the cluster that we should add to
			int closestCenterIndex = 0;
			double comparator = comparedValues.get(0);
			for (int i = 1; i < comparedValues.size(); i++) {
				if (comparedValues.get(i) < comparator) {
					closestCenterIndex = i;
				}
			}
			object.setclusterAssociation(closestCenterIndex);
			clusters.get(closestCenterIndex).add(object);
		}
		return clusters;
	}

	public static boolean compareClusters(List<List<DataObject>> cluster1,
			List<List<DataObject>> cluster2) {
		boolean result = false;
		if ((cluster1 != null) && (cluster2 != null)) {
			if (cluster1.size() == cluster2.size()) {
				result = true;
				for (int dataObjectListIndex = 0; dataObjectListIndex < cluster1
						.size(); dataObjectListIndex++) {
					if (cluster1.get(dataObjectListIndex).size() == cluster2
							.get(dataObjectListIndex).size()) {
						result = true;
						for (int dataObjectIndex = 0; dataObjectIndex < cluster1
								.get(dataObjectListIndex).size(); dataObjectIndex++) {
							if (!cluster1
									.get(dataObjectListIndex)
									.get(dataObjectIndex)
									.dataObjectIsEqual(
											cluster2.get(dataObjectListIndex)
													.get(dataObjectIndex))) {
								result = false;
								break;
							}
						}
					} else { // sizes are not equal
						result = false;
						break;
					}
				}
			}
		}
		return result;
	}

	public static boolean clusterContainsObjects(DataObject dataObject1,
			DataObject dataObject2, List<DataObject> cluster) {
		boolean hasFirst = false;
		boolean hasSecond = false;
		if (dataObject1.getLineNumber() == Utils.FAKE_POINT
				|| dataObject2.getLineNumber() == Utils.FAKE_POINT) {
			hasFirst = true;
			hasSecond = true;
		} else {
			for (DataObject dataObject : cluster) {

				hasFirst = dataObject.dataObjectIsEqual(dataObject1) || hasFirst;
				hasSecond = dataObject.dataObjectIsEqual(dataObject2) || hasSecond;
				if (hasFirst && hasSecond) {
					break;
				}
			}
		}
		return hasFirst && hasSecond;
	}

	public static List<List<DataObject>> createAverageCenters(
			List<List<DataObject>> clusters) {
		ArrayList<List<DataObject>> newClusters = new ArrayList<List<DataObject>>();
		for (List<DataObject> cluster : clusters) {
			ArrayList<DataObject> objects = new ArrayList<DataObject>();
			objects.add(findAverageDataObject(cluster));
			newClusters.add(objects);
		}
		return newClusters;
	}

	public static DataObject findAverageDataObject(List<DataObject> cluster) {
		ArrayList<Double> averagePoints = new ArrayList<Double>();
		for (int i = 0; i < cluster.get(0).getDimensions().size(); i++) {
			int counter = 0;
			double totalDimention = 0.0;
			for (DataObject dataObject : cluster) {
				counter++;
				totalDimention += dataObject.getDimensions().get(i);
			}
			averagePoints.add(totalDimention / (double) counter);
		}
		return new DataObject(averagePoints, FAKE_POINT);
	}

	public static List<ClusterObject> createClusterList(
			List<DataObject> dataList) {
		List<ClusterObject> clusterList = new ArrayList<ClusterObject>();
		List<DataObject> dataObjectList;
		for (DataObject dataObject : dataList) {
			dataObjectList = new ArrayList<DataObject>();
			dataObjectList.add(dataObject);
			clusterList.add(new ClusterObject(dataObjectList));
		}
		return clusterList;
	}

	public static double findClusterDistance(ClusterObject cluster1,
			ClusterObject cluster2) {
		double distance = 0.0;
		int itt = 0;
		for (DataObject data1 : cluster1.getClusterList()) {
			for (DataObject data2 : cluster2.getClusterList()) {
				itt++;
				distance += getEuclidianDistance(data1.getDimensions(),
						data2.getDimensions());
			}
		}
		return distance / itt;
	}

	public static ClusterObject[] findClosestClusters(
			List<ClusterObject> clusters) {
		ClusterObject[] combinedClusters = new ClusterObject[2];
		double closestCluster = 999999999.9;
		double compareClosest = 99999999999.0;
		for (int i = 0; i < clusters.size(); i++) {
			for (int j = 0; j < clusters.size(); j++) {
				if (!(clusters.get(i) == clusters.get(j))) {
					compareClosest = Utils.findClusterDistance(clusters.get(i),
							clusters.get(j));
					if (compareClosest < closestCluster) {
						closestCluster = compareClosest;
						combinedClusters[0] = clusters.get(i);
						combinedClusters[1] = clusters.get(j);
					}
				}
			}
		}
		return combinedClusters;
	}

	public static void printVectorRepresentation(
			ClustersContainerObject bestClustersObject, int kClusters,
			boolean isLloyds) {
		int arraySize = 0;
		arraySize = bestClustersObject.getSize();
		int clusterPositions[] = new int[arraySize /*- (isLloyds ? kClusters : 0)]*/];
		for (ClusterObject cluster : bestClustersObject.getClusters()) {
			for (DataObject object : cluster.getClusterList()) {
				if (object.getLineNumber() != Utils.FAKE_POINT) {
					clusterPositions[object.getLineNumber()] = object
							.getClusterAssociation();
				}
			}
		}
		System.out.print("[");
		for (int j = 0; j < clusterPositions.length; j++) {
			if (j == clusterPositions.length - 1) {
				System.out.println(clusterPositions[j] + "]");
			} else {
				System.out.print(clusterPositions[j] + ",");
			}
		}
	}
}
