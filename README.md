To execute the java code

1. Clone the remote repository to a local location
2. In the same file as the source code, run "javac *.java" to compile the java files
3. Run the program using the instructions below

java Activator <argument1> <argument2> <argument3>

<argument1> this is the file path to the data file or the filename if in the current running directory.

<argument2> this will be the ‘k’ clusters. typing 5 means 5 clusters, etc. 

<argument3> this is the name of the algorithm that will be run. there are three possible arguments. “average” will run the average-linkage algorithm. “kmeans” will run Lloyd’s heuristic k-means algorithm, and “ham” will run both algorithms and output the hamming distance between the two cluster groupings (kmeans vs average-linkage) along with the clusters produced by each algorithm. 

*********************************************************************************************************************************************************
*********************************************************************************************************************************************************
Sample command 1: (runs the average-linkage algorithm and produces 3 cluster groupings)
java Activator data_5D 3 average

Sample output for command 1: (*Note that each index represents the line (data object) from the data file. the numerical value represents the cluster grouping of that object.)
Total operational time for Average-Linkage in millis: 80
Avg-Li: [0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1]
 *********************************************************************************************************************************************************
Sample command 2: (runs lloyd's heuristic k-means algorithm and produces 5 cluster groupings)
java Activator data_5D 5 kmeans

Sample output 2: 
Total operational time for Lloyds in millis: 229
Lloyds: [3,4,3,3,3,4,4,3,2,4,4,0,2,4,4,4,1,3,3,0,2,4,4,0,2,4,1,0,2,4,4,2,2,4,4,3,2,1,3,0,2,4,4,2,1,4,4,4,4]
*********************************************************************************************************************************************************
Sample command 3:(runs both average-linkage and lloyd's, prints output, calculates and prints hamming distance)
java Activator data_5D 5 kmeans

Sample output 3: 
Total operational time for Lloyds in millis: 229
Lloyds: [3,4,3,3,3,4,4,3,2,4,4,0,2,4,4,4,1,3,3,0,2,4,4,0,2,4,1,0,2,4,4,2,2,4,4,3,2,1,3,0,2,4,4,2,1,4,4,4,4]
Jesse-Hannas-MacBook-Pro:ClusteringAlg jessehanna$ java Activator data_5D 5 ham

Total operational time for Lloyds in millis: 247
Lloyds: [2,4,2,1,1,4,4,0,3,4,4,3,3,4,4,4,3,2,2,0,1,4,4,0,3,4,3,3,4,4,4,3,3,4,4,0,1,2,0,0,3,4,4,3,3,4,4,4,4]

Total operational time for Average-Linkage in millis: 57
Avg-Li: [0,4,4,3,3,4,4,2,3,4,4,3,3,4,4,4,2,4,4,4,3,4,4,4,3,4,2,3,1,4,4,3,3,4,4,4,3,4,2,4,3,4,4,3,2,4,4,4,1]
Hamming Distance = 0.25255102040816324
*********************************************************************************************************************************************************

data_1D.txt: a one dimensional data set
data_2D.txt: a two dimensional data set
data_3D.txt: a three dimensional data set
data_5D.txt: a five dimensional data set

*********************************************************************************************************************************************************
*********************************************************************************************************************************************************
For questions or comments please email: jessehanna@tampabay.rr.com
*********************************************************************************************************************************************************
*********************************************************************************************************************************************************
