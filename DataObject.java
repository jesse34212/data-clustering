
import java.util.ArrayList;

public class DataObject {
	private int lineNumber;
	private int clusterAssociation;
	private double EPSILON = 0.000001;
	private boolean center = false;
	ArrayList<Double> dataPoints;
	DataObject(ArrayList<Double> dataPoints, int lineNumber){
		this.dataPoints = dataPoints;
		this.lineNumber = lineNumber;
	}
	public void setCenter(boolean center){
		this.center = center;
	}
	
	public boolean isCenter(){
		return center;
	}
	
	public ArrayList<Double> getDimensions(){
		return this.dataPoints;
	}
	
	public void setclusterAssociation(int clusterAssociation){
		this.clusterAssociation = clusterAssociation;
	}
	public int getClusterAssociation(){
		return clusterAssociation;
	}
	
	public void setLineNumber(int lineNumber){
		this.lineNumber = lineNumber;
	}
	
	public int getLineNumber(){
		//line number can be mapped to the index of the print vector
		return this.lineNumber;
	}
	
	public boolean dataObjectIsEqual(DataObject dataObject){
		boolean result = true;
		if (getDimensions().size() == dataObject.getDimensions().size()){
			for (int i = 0; i < getDimensions().size(); i++){
				if (Math.abs(getDimensions().get(i) - dataObject.getDimensions().get(i)) > EPSILON){
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	@Override
	public String toString(){
		return "(" + clusterAssociation + ")" + dataPoints.toString();
	}
}
